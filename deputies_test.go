package opencam

import (
	"reflect"
	"testing"
)

// Struct test GetDeputiesByName
type depNameTest struct {
	input    string
	expected []BasicDeputies
}

// Struct test GetDetailsDeputies
type depIDTest struct {
	input    int
	expected DetailsDeputy
}

func TestGetDeputiesByName(t *testing.T) {
	test1 := []depNameTest{
		{
			input: "Maria",
			expected: []BasicDeputies{
				{
					74398,
					"https://dadosabertos.camara.leg.br/api/v2/deputados/74398",
					"MARIA DO ROSÁRIO",
					"PT",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36844",
					"RS",
					55,
					"http://www.camara.leg.br/internet/deputado/bandep/74398.jpg",
				},
				{
					64960,
					"https://dadosabertos.camara.leg.br/api/v2/deputados/64960",
					"MARIA HELENA",
					"MDB",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36899",
					"RR",
					55,
					"http://www.camara.leg.br/internet/deputado/bandep/64960.jpg",
				},
				{
					178956,
					"https://dadosabertos.camara.leg.br/api/v2/deputados/178956",
					"MARIANA CARVALHO",
					"PSDB",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36835",
					"RO",
					55,
					"http://www.camara.leg.br/internet/deputado/bandep/178956.jpg",
				},
				{
					141507,
					"https://dadosabertos.camara.leg.br/api/v2/deputados/141507",
					"MAURO MARIANI",
					"MDB",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36899",
					"SC",
					55,
					"http://www.camara.leg.br/internet/deputado/bandep/141507.jpg",
				},
			},
		},
		{
			input: "sérgio reis",
			expected: []BasicDeputies{
				{
					178991,
					"https://dadosabertos.camara.leg.br/api/v2/deputados/178991",
					"SÉRGIO REIS",
					"PRB",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36815",
					"SP",
					55,
					"http://www.camara.leg.br/internet/deputado/bandep/178991.jpg",
				},
			},
		},
	}
	d := Deputies{}
	for _, itest := range test1 {
		current := d.GetDeputiesByName(itest.input)
		if !reflect.DeepEqual(current, itest.expected) {
			t.Errorf("\nExpected value:\n%+v\nOutput:\n%+v", itest.expected, current)
		}
	}
}

func TestGetDetailsDeputies(t *testing.T) {
	test1 := []depIDTest{
		{
			input: 73441,
			expected: DetailsDeputy{
				73441,
				"https://dadosabertos.camara.leg.br/api/v2/deputados/73441",
				"CELSO UBIRAJARA RUSSOMANNO",
				lastStatus{
					73441,
					"https://dadosabertos.camara.leg.br/api/v2/deputados/73441",
					"CELSO RUSSOMANNO",
					"PRB",
					"https://dadosabertos.camara.leg.br/api/v2/partidos/36815",
					"SP",
					55,
					"http://www.camara.leg.br/internet/deputado/bandep/73441.jpg",
					"2015-02-01",
					"CELSO RUSSOMANNO",
					cabinet{
						"960",
						"4",
						"960",
						"9",
						"3215-5960",
						"dep.celsorussomanno@camara.leg.br",
					},
					"Exercício",
					"Titular",
					"",
				},
				"",
				"M",
				"",
				[]string{},
				"1956-08-20",
				"",
				"SP",
				"São Paulo",
				"Superior",
			},
		},
	}
	d := Deputies{}
	for _, itest := range test1 {
		current := d.GetDetailsDeputy(itest.input)
		if !reflect.DeepEqual(current, itest.expected) {
			t.Errorf("\nExpected value:\n%+v\nOutput:\n%+v", itest.expected, current)
		}
	}

}
