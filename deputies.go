package opencam

import (
	"fmt"
	"net/url"
)

// Deputies contains all the function Deputies
type Deputies struct {
}

// BASIC INFORMATION ON DEPUTIES:

// BasicDeputies contains basic information about deputies
type BasicDeputies struct {
	ID            int    `json:"id"`
	URI           string `json:"uri"`
	Name          string `json:"nome"`
	AcronymParty  string `json:"siglaPartido"`
	URIParty      string `json:"uriPartido"`
	UF            string `json:"siglaUf"`
	IDlegislature int    `json:"idLegislatura"`
	URLPhoto      string `json:"urlFoto"`
}

type elementalDeputies struct {
	Data []BasicDeputies `json:"dados"`
}

// GetDeputiesByName list and search for deputies according to criteria - sorted by name
func (d Deputies) GetDeputiesByName(name string) []BasicDeputies {
	urlName := &url.URL{Path: name}
	url := fmt.Sprintf("%sdeputados?nome=%s&ordenarPor=nome", BaseURL, urlName)
	var basicInfo elementalDeputies
	getResponse(url, &basicInfo)
	return basicInfo.Data
}

// DETAILED INFORMATION OF A DEPUTY

type cabinet struct {
	Name     string `json:"nome"`
	Building string `json:"predio"`
	Room     string `json:"sala"`
	Floor    string `json:"andar"`
	Phone    string `json:"telefone"`
	Email    string `json:"email"`
}

type lastStatus struct {
	ID                 int     `json:"id"`
	URI                string  `json:"uri"`
	Name               string  `json:"nome"`
	AcronymParty       string  `json:"siglaPartido"`
	URIPArty           string  `json:"uriPartido"`
	AcronymUF          string  `json:"siglaUf"`
	IDLegislatura      int     `json:"idLegislatura"`
	URLPhoto           string  `json:"urlFoto"`
	Date               string  `json:"data"`
	ElectoralName      string  `json:"nomeEleitoral"`
	Cabinet            cabinet `json:"gabinete"`
	Situation          string  `json:"situacao"`
	ElectoralCondition string  `json:"condicaoEleitoral"`
	StatusDescription  string  `json:"descricaoStatus"`
}

// DetailsDeputy contains all the information of a deputy
type DetailsDeputy struct {
	ID                int        `json:"id"`
	URI               string     `json:"uri"`
	CivilName         string     `json:"nomeCivil"`
	LastStatus        lastStatus `json:"ultimoStatus"`
	CPF               string     `json:"cpf"`
	Gender            string     `json:"sexo"`
	URLWebsite        string     `json:"urlWebsite"`
	SocialNetwork     []string   `json:"redeSocial"`
	BirthDate         string     `json:"dataNascimento"`
	DeathDate         string     `json:"dataFalecimento"`
	BirthUF           string     `json:"ufNascimento"`
	MunicipalityBirth string     `json:"municipioNascimento"`
	Schooling         string     `json:"escolaridade"`
}

type fullDeputy struct {
	Data DetailsDeputy `json:"dados"`
}

// GetDetailsDeputy Complete information about a Deputy
// Returns a struct DetailsDeputy
func (d Deputies) GetDetailsDeputy(id int) DetailsDeputy {
	url := fmt.Sprintf("%sdeputados/%d", BaseURL, id)
	var detailsInfo fullDeputy
	getResponse(url, &detailsInfo)
	return detailsInfo.Data
}
