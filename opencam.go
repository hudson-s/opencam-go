package opencam

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	// BaseURL API <Camara Dados Abertos API>
	BaseURL = "https://dadosabertos.camara.leg.br/api/v2/"
)

func getResponse(url string, dataResult interface{}) {
	httpClient := &http.Client{
		Timeout: time.Second * 2,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		fmt.Print(err.Error())
	}

	req.Header.Set("accept", "application/json")

	res, err := httpClient.Do(req)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer res.Body.Close()

	resData, _ := ioutil.ReadAll(res.Body)

	json.Unmarshal(resData, &dataResult)
}
